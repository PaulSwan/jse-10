package ru.lebedev.tm.service;

import ru.lebedev.tm.entity.Project;
import ru.lebedev.tm.entity.Task;
import ru.lebedev.tm.repository.ProjectRepository;
import ru.lebedev.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectTaskService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;


    public ProjectTaskService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Task removeTaskToProject(final Long projectId, final Long taskId){
        final Task task = taskRepository.findByProjectIdandId(projectId, taskId);
        if(task == null) return null;
        task.setProjectId(null);
        return task;
    }

    public Task addTaskToProject(final Long projectId, final Long taskId){
        final Project project = projectRepository.findById(projectId);
        if(project == null) return null;
        final Task task = taskRepository.findById(taskId);
        if(task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

}
